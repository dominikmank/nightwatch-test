module.exports = {
    environment: 'local',
    abortOnAssertionFailure: true,
    waitForConditionTimeout: 5000,
    retryAssertionTimeout: 5000
};
