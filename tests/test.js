module.exports = {
    after: (browser) => {
        browser.closeWindow();
        browser.end();
    },
    'get test element': (browser) => {
        let foo = '';
        browser.url('https://material.angular.io/components/select/overview')
            .waitForElementVisible('div[material-docs-example="select-overview"]', 10000)
            .click('div[material-docs-example="select-overview"] div.docs-example-viewer-body select-overview-example')
            .execute(function(done) {
                return document.querySelector('div[material-docs-example="select-overview"] div.docs-example-viewer-body select-overview-example').attributes;
            }, [], function(result) {
                let filteredResult = result.value.filter(attribute => attribute.name.includes('_nghost'));
                if (filteredResult) {
                    let splitted = filteredResult[0].name.split('-');
                    foo = '_ngcontent-' + splitted[splitted.length - 1];
                }
            })
            .perform(function(client, done) {
                client.verify.visible('mat-option[' + foo + '=""]')
                    .verify.elementPresent('mat-option[' + foo + '=""] > span')
                    .verify.containsText('mat-option[' + foo + '=""] > span', 'Steak')
                    .pause(100);
                done();
            })
            .end();
    },
};
